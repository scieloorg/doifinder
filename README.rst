doifinder
=========

    API to recover DOI number from a given metadata.

How to install
==============

    pip install doifinder



How to use
==========

code::

    Python 2.7.3 |EPD_free 7.3-2 (32-bit)| (default, Apr 12 2012, 11:28:34)
    [GCC 4.0.1 (Apple Inc. build 5493)] on darwin
    Type "credits", "demo" or "enthought" for more information.
    >>> from doifinder.doifinder import DoiFinder
    >>> df = DoiFinder(user="xxx", passwd="xxxxxx")
    >>> meta = {'key': 'free_data', 'email_address': 'crossref@crossref.org', 'doi_batch_id': 'crossref', 'issn': '1414-431X', 'journal_title': 'Brazilian Journal of Medical and Biological Research', 'article_title': 'Expression and purification of the immunogenically active fragment B of the Park Williams 8 Corynebacterium diphtheriae strain toxin', 'author': 'Nascimento, D.V.', 'year': '2010', 'volume': '43', 'issue': '5', 'first_page': '460'}
    >>> df.find_doi(**meta)
    '10.1590/S0100-879X2010007500032'
